#!/bin/bash

export RELEASE=$CIRCLE_TAG$CIRCLE_BRANCH/#$CIRCLE_BUILD_NUM

# Development (develop branch)
if [[ $CIRCLE_BRANCH == 'develop' ]]; then
  export CONFIG_ENV=dev
  export NODE_ENV=production
  export BABEL_ENV=production
  export FIREBASE_PROJECT=cebula-dev
fi

# Staging (v1-a, v1.2-alpha, and v1.2.3-rc (and so on) all match)
if [[ $CIRCLE_TAG =~ ^v[0-9]+(\.[0-9]+)*-(a|alpha|rc)$ ]]; then
  export CONFIG_ENV=stage
  export NODE_ENV=production
  export BABEL_ENV=production
  export FIREBASE_PROJECT=cebula-stage
fi

# Production (v1, v1.2, and v1.2.3 (and so on) all match)
if [[ $CIRCLE_TAG =~ ^v[0-9]+(\.[0-9]+)*$ ]]; then
  export CONFIG_ENV=prod
  export NODE_ENV=production
  export BABEL_ENV=production
  export FIREBASE_PROJECT=cebula-prod
fi

yarn run build
yarn run deploy:ci
