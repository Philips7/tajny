import Loadable from 'react-loadable'
import * as React from 'react'

export const Notifications = Loadable.Map({
  loader: {
    NotificationsModule: () => import(/* webpackChunkName: "Component Notifications" */ './components/Notifications'),
  },
  loading: () => null,
  render: ({ NotificationsModule: { Notifications } }: any, props) => (<Notifications {...props} />),
})
