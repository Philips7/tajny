import { createReducer } from 'utils/createReducer'
import { IState } from 'types'
import { IMessage, INotificationsInitialState } from '../types'

// ------------------------------------
// Constants
// ------------------------------------
export const SHOW = 'Notifications.SHOW'
export const HIDE = 'Notifications.HIDE'

// ------------------------------------
// Actions
// ------------------------------------
export const show = (text: string, messageType: string = 'info', delay: number = 2000) => (dispatch: any) => {
  const id = Math.random().toString(16)
  dispatch({
    type: SHOW,
    payload: {
      id,
      text,
      messageType,
      delay,
    } as IMessage,
  })

  setTimeout(() => dispatch({
    type: HIDE,
    id,
  }), delay)
}

// ------------------------------------
// Selectors
// ------------------------------------
export const getNotifications = (state: IState): INotificationsInitialState => state.notifications || initialState

// ------------------------------------
// Reducer
// ------------------------------------

const initialState: INotificationsInitialState = {
  messages: [],
}

export const reducer = createReducer<INotificationsInitialState>('notifications', initialState, {
  [SHOW]: (state: INotificationsInitialState, { payload }: { payload: IMessage, }) => ({
    messages: [...state.messages, payload],
  }),
  [HIDE]: (state: INotificationsInitialState, { id }: { id: string }) => ({
    messages: state.messages.filter((message: IMessage) => message.id !== id),
  }),
})
