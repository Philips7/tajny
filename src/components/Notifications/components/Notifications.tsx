import * as React from 'react'
import { connect } from 'react-redux'
import { getNotifications } from '../redux/notifications'
import { INotificationsProps } from '../types'
import { IState } from 'types'
import * as cn from 'classnames'

const css = require('./notifications.scss')

class NotificationsComponent extends React.Component<INotificationsProps, any> {
  render () {
    const { messages } = this.props

    return (
      <div className={css.container}>
        {messages.map(({ id, messageType, text }, i) => (
          <div
            key={id}
            className={cn({
              [css.info]: messageType === 'info',
            })}
          >
            {text}
          </div>
        ))}
      </div>
    )
  }
}

export const Notifications = connect(
  (state: IState) => ({
    messages: getNotifications(state).messages,
  }),
  {},
)(NotificationsComponent as any)
