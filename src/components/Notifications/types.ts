import { Action } from 'redux'

export interface IMessage {
  id: string,
  text: string,
  messageType: 'info' | 'error',
  delay: number,
}

export interface INotificationsInitialState {
  messages: IMessage[],
}

export interface INotificationsProps {
  messages: IMessage[]
}

export interface IShowPayload extends IMessage, Action {

}
