import * as React from 'react'
import { Header } from 'components/Header/Header'
import { Notifications } from 'components/Notifications'

const c = require('./defaultLayout.scss')

export const DefaultLayout = ({ children }: any): any => (
  <div className={c.container}>
    <Notifications/>
    <Header/>
    {children}
  </div>
)
