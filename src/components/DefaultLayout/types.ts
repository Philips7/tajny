export interface IHeaderProps {
  isAuthenticated: boolean,
  authAttempt: () => any,
  authLogout: () => any,
}
