import * as React from 'react'

const css = require('./loader.scss')
const onion = require('../assets/onion.jpg')

export const Loader: React.StatelessComponent<{}> = () => (
  <div className={css.container}>
    <img src={onion} className={css.plane}/>
  </div>
)
