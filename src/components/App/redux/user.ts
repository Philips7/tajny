import { createReducer } from 'utils/createReducer'
import { WebAuth } from 'auth0-js'
import { auth0callback, home as homeUrl } from 'utils/url'
import { replace } from 'react-router-redux'
import { getCredentials, ICredentials, removeCredentials, setCredentials } from 'utils/auth'
import { IUserInitialState } from '../types'
import { IState } from 'types'

// ------------------------------------
// Constants
// ------------------------------------
export const AUTH_ATTEMPT = 'User.AUTH_ATTEMPT'
export const AUTH_SUCCESS = 'User.AUTH_SUCCESS'
export const AUTH_LOGOUT = 'User.AUTH_LOGOUT'

const AUTH_CONFIG = {
  domain: '7ninjas.eu.auth0.com',
  clientId: 'e7LJhkkiE10BzRF8spcrtPtJ5a3U5Eq4',
  callbackUrl: `http://localhost:3000${auth0callback()}`,
}

const auth0 = new WebAuth({
  domain: AUTH_CONFIG.domain,
  clientID: AUTH_CONFIG.clientId,
  redirectUri: AUTH_CONFIG.callbackUrl,
  audience: `https://${AUTH_CONFIG.domain}/userinfo`,
  responseType: 'token id_token',
  scope: 'openid',
})

// ------------------------------------
// Actions
// ------------------------------------
export const checkAuth = () => (dispatch: any): void => {
  auth0.parseHash((err, authResult) => {
    if (authResult && authResult.accessToken && authResult.idToken) {
      dispatch(authSuccess({
        accessToken: authResult.accessToken,
        expiresIn: authResult.expiresIn,
        tokenType: authResult.tokenType,
        idToken: authResult.idToken,
      }))
    } else {
      const { accessToken, expiresIn, tokenType, idToken } = getCredentials()

      if (accessToken && expiresIn && tokenType && idToken) {
        dispatch(authSuccess({ accessToken, expiresIn, tokenType, idToken }))
      }
    }
  })
}

export const authAttempt = () => (): void => {
  auth0.authorize()
}

export const authSuccess = (hash: ICredentials) => (dispatch: any, getState: () => IState) => {
  const { location } = getState().routing
  setCredentials(hash)

  dispatch({ type: AUTH_SUCCESS, hash })

  if (location && location.pathname) {
    location.pathname === auth0callback() && dispatch(replace(homeUrl()))
  }
}

export const authLogout = () => {
  removeCredentials()
  return { type: AUTH_LOGOUT }
}

// ------------------------------------
// Selectors
// ------------------------------------
export const getUser = (state: IState) => state.user || initialState

// ------------------------------------
// Reducer
// ------------------------------------
const initialState: IUserInitialState = {
  isAuthenticated: false,
  accessToken: '',
  expiresIn: -1,
  tokenType: '',
  idToken: '',
}

export const reducer = createReducer<IUserInitialState>('user', initialState, {
  [AUTH_SUCCESS]: (state, { accessToken, expiresIn, tokenType, idToken }: ICredentials) => ({
    isAuthenticated: true,
    accessToken: accessToken || initialState.accessToken,
    expiresIn: expiresIn || initialState.expiresIn,
    tokenType: tokenType || initialState.tokenType,
    idToken: idToken || initialState.idToken,
  }),
  [AUTH_LOGOUT]: () => initialState,
})
