import * as React from 'react'
import './app.scss'
import { Route, Switch } from 'react-router-dom'
import { ConnectedRouter } from 'react-router-redux'
import { auth0callback as auth0callbackUrl, example as exampleUrl, home as homeUrl } from 'utils/url'
import { Example } from 'routes/Example'
import { Home } from 'routes/Home'
import { AuthCallback } from 'routes/AuthCallback'
import { NotFound } from 'routes/NotFound'
import { history } from 'utils/store'
import { connect } from 'react-redux'
import { checkAuth } from '../redux/user'
import { logError } from 'utils/errorHandler'

class AppComponent extends React.Component<any, any> {
  componentWillMount () {
    this.props.checkAuth()
  }

  componentDidCatch (e: Error) {
    logError(e)
  }

  render (): any {
    return (
      <div>
        <ConnectedRouter history={history}>
          <Switch>
            <Route exact={true} component={Home as any} path={homeUrl()}/>
            <Route exact={true} component={AuthCallback as any} path={auth0callbackUrl()}/>
            <Route exact={true} component={Example as any} path={exampleUrl()}/>
            <Route component={NotFound as any}/>
          </Switch>
        </ConnectedRouter>
      </div>
    )
  }
}

export const App = connect(() => ({}), { checkAuth })(AppComponent)
