export interface IUserInitialState {
  isAuthenticated: boolean,
  accessToken: string,
  expiresIn: number,
  tokenType: string,
  idToken: string,
}