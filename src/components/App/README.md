# Important

## It's entry file which shouldn't import any `no-async` components. It's not file where you can create layout, only global logic like auth/routing is allowed here