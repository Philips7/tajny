import * as React from 'react'
import { connect } from 'react-redux'
import { authAttempt, authLogout, getUser } from 'components/App/redux/user'
import { IState } from 'types'
import { IHeaderProps } from './types'

const c = require('./header.scss')

const HeaderComponent: React.StatelessComponent<IHeaderProps> = ({ authAttempt, authLogout, isAuthenticated }) => (
  <div className={c.container}>
    {!isAuthenticated && (
      <button type='button' onClick={authAttempt}>
        Log In
      </button>
    )}

    {isAuthenticated && (
      <button type='button' onClick={authLogout}>
        Log Out
      </button>
    )}
  </div>
)

export const Header = connect((state: IState) => ({
  isAuthenticated: getUser(state).isAuthenticated,
}), { authLogout, authAttempt })(HeaderComponent)
