import * as React from 'react'
import pure from 'recompose/pure'
import * as cn from 'classnames'
import { IInputDefaultProps, IInputProps } from '../types'

const TextareaComponent: React.StatelessComponent<IInputProps> = ({
  input,
  label,
  meta: { error, touched },
  disabled,
  placeholder,
  id,
  classes,
}) => (
  <div className={cn(classes.container, { [classes.hasError]: error && touched })}>
    {label
    && <label
      htmlFor={id}
      className={classes.label}
    >
      {label}
    </label>}

    <textarea
      {...input}
      id={id}
      className={cn(classes.input)}
      disabled={disabled}
      placeholder={placeholder}
    >
      {input.value || placeholder}
    </textarea>

    {(error && touched) && <div className={classes.errorFeedback}>{error}</div>}
  </div>
)

TextareaComponent.defaultProps = {
  classes: {
    container: '',
    label: '',
    input: '',
    hasError: '',
    errorFeedback: '',
  },
  disabled: false,
  placeholder: '',
} as IInputDefaultProps

export const Textarea = pure(TextareaComponent) as React.StatelessComponent<any>
