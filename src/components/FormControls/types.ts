import { ReactNode } from 'react'
import { WrappedFieldProps } from 'redux-form'
import { ReactSelectProps } from 'react-select'

export interface IClasses {
  container: string,
  label: string,
  input: string,
  hasError: string,
  errorFeedback: string,
}

export interface IInputDefaultProps {
  disabled: boolean,
  placeholder: string,
  classes: IClasses,
  selectSettings: ReactSelectProps,
}

export interface IInputProps extends IInputDefaultProps, WrappedFieldProps {
  label: ReactNode,
  type: 'text' | 'number',
  id: string,
}

export interface ISelectOption {
  label: string,
  value: string | number,
}

export interface ISelectProps extends IInputDefaultProps, WrappedFieldProps {
  options: ISelectOption[],
  label: ReactNode,
  id: string,
  selectSettings: ReactSelectProps,
}
