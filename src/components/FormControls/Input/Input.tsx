import * as React from 'react'
import pure from 'recompose/pure'
import * as cn from 'classnames'
import { IInputDefaultProps, IInputProps } from '../types'

const c = require('./input.scss')

const InputComponent: React.StatelessComponent<IInputProps> = ({
  input,
  label,
  type,
  meta: { error, touched },
  disabled,
  placeholder,
  id,
  classes,
}) => (
  <div className={cn(classes.container, { [classes.hasError]: error && touched })}>
    {label
    && <label
      htmlFor={id}
      className={classes.label}
    >
      {label}
    </label>}

    <input
      {...input}
      id={id}
      className={cn(classes.input)}
      type={type}
      disabled={disabled}
      placeholder={placeholder}
    />

    {(error && touched) && <div className={classes.errorFeedback}>{error}</div>}
  </div>
)

InputComponent.defaultProps = {
  classes: {
    container: '',
    label: '',
    input: '',
    hasError: '',
    errorFeedback: c.errorFeedback,
  },
  disabled: false,
  placeholder: '',
} as IInputDefaultProps

export const Input = pure(InputComponent) as React.StatelessComponent<any>
