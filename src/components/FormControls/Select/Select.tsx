import * as React from 'react'
import pure from 'recompose/pure'
import * as cn from 'classnames'
import { IInputDefaultProps, ISelectOption, ISelectProps } from '../types'
import ReactSelect from 'react-select'

const c = require('./select.scss')

class SelectComponent extends React.Component<ISelectProps, any> {
  static defaultProps: IInputDefaultProps = {
    classes: {
      container: '',
      label: '',
      input: '',
      hasError: '',
      errorFeedback: '',
    },
    disabled: false,
    placeholder: '',
    selectSettings: {
      onBlurResetsInput: false,
      clearable: false,
    },
  }

  onChange = ({ value }: ISelectOption) => {
    this.props.input.onChange(value)
  }

  onBlur = () => {
    this.props.input.onBlur(this.props.input.value)
  }

  render () {
    const {
      input,
      label,
      meta: { error, touched },
      id,
      classes,
      options,
      selectSettings,
    } = this.props

    return (
      <div className={cn(c.select, classes.container, { [classes.hasError]: error && touched })}>
        {label
        && <label
          htmlFor={id}
          className={classes.label}
        >
          {label}
        </label>}

        <ReactSelect
          {...input}
          {...selectSettings}
          onChange={this.onChange}
          onBlur={this.onBlur}
          className={cn(classes.input)}
          options={options}
        />

        {(error && touched) && <div className={classes.errorFeedback}>{error}</div>}
      </div>
    )
  }
}

export const Select = pure(SelectComponent) as React.StatelessComponent<any>
