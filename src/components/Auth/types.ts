export interface IAuthProps {
  isAuthenticated: boolean,
  show: (message: string) => any,
}
