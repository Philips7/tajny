import * as React from 'react'
import { connect } from 'react-redux'
import { IState } from '../../types'
import { getUser } from '../App/redux/user'
import { Redirect } from 'react-router-dom'
import { home } from 'utils/url'
import { show } from 'components/Notifications/redux/notifications'
import { IAuthProps } from './types'

const AuthComponent: React.StatelessComponent<IAuthProps> = ({ children, isAuthenticated, show }) => {
  if (!isAuthenticated) {
    show('Only authenticated members can see this component')
    return <Redirect to={home()}/>
  }

  return React.Children.only(children)
}

export const Auth = connect((state: IState) => ({
  isAuthenticated: getUser(state).isAuthenticated,
}), { show })(AuthComponent as any)
