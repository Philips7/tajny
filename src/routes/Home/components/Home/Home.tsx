import * as React from 'react'
import { connect } from 'react-redux'
import { authAttempt, getUser } from 'components/App/redux/user'
import { IState } from 'types'
import { IHomeProps } from '../../types'
import { DefaultLayout } from 'components/DefaultLayout/DefaultLayout'
import { Link } from 'react-router-dom'
import { example } from 'utils/url'

const HomeComponent: React.StatelessComponent<IHomeProps> = ({
  isAuthenticated,
  authAttempt,
}) => (
  <DefaultLayout>
    Hołm
    <div className='container'>
      {isAuthenticated &&
      <div>
        <h1>You are logged in!</h1>
        <Link to={example()}>visit example page</Link>
      </div>}

      {!isAuthenticated &&
      <div>
        <h4>
          You are not logged in! Please{' '}
          <button type='button' onClick={authAttempt}>
            Log In
          </button>
          {' '}
          to continue.
        </h4>

        or try to visit protected url{' '}
        <Link to={example()}>visit example page</Link>
      </div>}
    </div>
  </DefaultLayout>
)

export const Home = connect(
  (state: IState) => ({
    isAuthenticated: getUser(state).isAuthenticated,
  }),
  {
    authAttempt,
  },
)(HomeComponent as any)
