import Loadable from 'react-loadable'
import * as React from 'react'
import { Loader } from 'components/Loader'

export const Home = Loadable.Map({
  loader: {
    HomeModule: () => import(/* webpackChunkName: "Route Home" */ './components/Home'),
  },
  loading: Loader,
  render: ({ HomeModule: { Home } }: any, props) => <Home {...props} />,
})
