import * as React from 'react'
import { connect } from 'react-redux'
import { authAttempt } from 'components/App/redux/user'

const style = {
  position: 'absolute',
  display: 'flex',
  justifyContent: 'center',
  height: '100vh',
  width: '100vw',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  backgroundColor: 'white',
}

class AuthCallbackComponent extends React.Component<any, any> {
  componentWillMount () {
    this.props.authAttempt()
  }

  render (): any {
    return (
      <div style={style as any}>
        loading
      </div>
    )
  }
}

export const AuthCallback = connect(() => ({}), { authAttempt })(AuthCallbackComponent)
