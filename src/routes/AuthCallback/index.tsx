import Loadable from 'react-loadable'
import * as React from 'react'
import { Loader } from 'components/Loader'

export const AuthCallback = Loadable.Map({
  loader: {
    AuthCallbackModule: () => import(/* webpackChunkName: "Route AuthCallback" */ './components/AuthCallback/AuthCallback'),
  },
  loading: Loader,
  render: ({ AuthCallbackModule: { AuthCallback } }: any, props) => (<AuthCallback {...props} />),
})
