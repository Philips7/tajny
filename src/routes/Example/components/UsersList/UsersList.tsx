import * as React from 'react'
import { IUsersListProps } from '../../types'
import { Loader } from 'components/Loader'

const c = require('./usersList.scss')

export const UsersList: React.StatelessComponent<IUsersListProps> = ({
  users,
  showLoader,
}) => (
  <div className={c.table}>
    {showLoader && <Loader/>}

    <div className={c.row}>
      <div className={c.cell}>First Name</div>
      <div className={c.cell}>Last Name</div>
      <div className={c.cell}>Type</div>
      <div className={c.cell}>Description</div>
    </div>

    {users.map(({ firstName, lastName, type, description }, i) => (
      <div className={c.row} key={`${firstName}${i}`}>
        <div className={c.cell}>{firstName}</div>
        <div className={c.cell}>{lastName}</div>
        <div className={c.cell}>{type}</div>
        <div className={c.cell}>{description}</div>
      </div>
    ))}
  </div>
)
