import * as React from 'react'
import { connect } from 'react-redux'
import { authAttempt, getUser } from 'components/App/redux/user'
import { IState } from 'types'
import { IExampleFormValues, IExampleProps } from '../../types'
import { DefaultLayout } from 'components/DefaultLayout/DefaultLayout'
import { Field, reduxForm } from 'redux-form'
import { Input } from 'components/FormControls/Input'
import {
  getExample, getNewUsersAttempt, getUsersSelector, selectProps,
  submitExampleFormAttempt,
} from '../../redux/example'
import { UsersList } from '../UsersList'
import { Select } from 'components/FormControls/Select'
import { Textarea } from 'components/FormControls/Textarea'
import { Auth } from 'components/Auth'

const c = require('./example.scss')

const ExampleComponent: React.StatelessComponent<IExampleProps> = ({
  handleSubmit,
  users,
  getNewUsersAttempt,
  showLoader,
}) => (
  <Auth>
    <DefaultLayout>
      <form
        onSubmit={handleSubmit}
        className={c.form}
      >
        <Field
          id='example.firstName'
          name='firstName'
          component={Input}
          label='First Name'
        />

        <Field
          id='example.lastName'
          name='lastName'
          component={Input}
          label='Last Name'
        />

        <Field
          id='example.type'
          name='type'
          component={Select}
          label='Type'
          props={selectProps as any}
        />

        <Field
          id='example.description'
          name='description'
          component={Textarea}
          label='Description'
          props={selectProps as any}
        />

        <button type='submit'>
          add user
        </button>
      </form>

      <UsersList
        users={users}
        showLoader={showLoader}
      />

      <button
        type='button'
        onClick={getNewUsersAttempt}
      >
        load new users
      </button>
    </DefaultLayout>
  </Auth>
)

export const Example = connect(
  (state: IState) => ({
    isAuthenticated: getUser(state).isAuthenticated,
    showLoader: getExample(state).showLoader,
    users: getUsersSelector(state),
    initialValues: {
      firstName: '',
      lastName: '',
      type: -1,
    },
    validate: (data: IExampleFormValues) => {
      const error: any = {}

      if (!data.firstName) {
        error.firstName = 'First name is required'
      }

      if (!data.lastName) {
        error.lastName = 'Last name is required'
      }

      return error
    },
  }),
  {
    getNewUsersAttempt,
    authAttempt,
    onSubmit: submitExampleFormAttempt,
  },
)(reduxForm({ form: 'example' })(ExampleComponent as any) as any)
