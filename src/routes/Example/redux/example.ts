import { createReducer } from 'utils/createReducer'
import { IState } from 'types'
import {
  IExampleForm, IExampleFormValues, IExampleInitialState, IGetUsers, IGetUsersSuccess,
  ISubmitExampleFormSuccessPayload,
} from '../types'
import { reset } from 'redux-form'
import { createSelector } from 'reselect'
import { ISelectOption } from 'components/FormControls/types'

// ------------------------------------
// Constants
// ------------------------------------
export const SUBMIT_EXAMPLE_FORM_SUCCESS = 'Example.SUBMIT_EXAMPLE_FORM_SUCCESS'
export const GET_USERS_ATTEMPT = 'Example.GET_USERS_ATTEMPT'
export const GET_USERS_SUCCESS = 'Example.GET_USERS_SUCCESS'

export const selectProps: { options: ISelectOption[] } = {
  options: [
    { value: 1, label: 'Frontend' },
    { value: 2, label: 'Backend' },
    { value: 3, label: 'Dizajner' },
  ],
}

// ------------------------------------
// Actions
// ------------------------------------
export const submitExampleFormAttempt = (data: IExampleFormValues) => (dispatch: any) => {
  dispatch(reset('example'))
  dispatch({
    type: SUBMIT_EXAMPLE_FORM_SUCCESS,
    payload: data,
  } as ISubmitExampleFormSuccessPayload)
}

export const getNewUsersAttempt = () => async (dispatch: any, getState: any, { get }: { get: any }) => {
  dispatch({ type: GET_USERS_ATTEMPT })
  const users = await get({
    path: 'user',
    baseUrl: 'https://5a43649d342c490012f3fc29.mockapi.io/',
    customHeaders: {
      'Content-Type': 'application/json',
    },
  })
  dispatch(getUsersSuccess(users))
}

export const getUsersSuccess: IGetUsersSuccess = users => ({
  type: GET_USERS_SUCCESS,
  users,
})

// ------------------------------------
// Selectors
// ------------------------------------
export const getExample = (state: IState) => state.example || initialState
export const getExampleFormValues = (state: IState) => state.form || exampleFormInitialState

export const getUsersSelector = createSelector<IState, IExampleFormValues[], IGetUsers[]>(
  [state => getExample(state).users],
  users => users.map(user => {
    const userType = selectProps.options.find(option => option.value === user.type)
    return {
      ...user,
      type: userType ? userType.label : 'none',
    }
  }),
)

// ------------------------------------
// Reducer
// ------------------------------------

const exampleFormInitialState: IExampleForm = {
  values: {
    firstName: '',
    lastName: '',
    description: '',
    type: -1,
  },
}

const initialState: IExampleInitialState = {
  showLoader: false,
  users: [
    {
      firstName: 'Arek',
      lastName: 'Rado',
      description: '💖💖💗💝💓😻💌',
      type: 1,
    },
    {
      firstName: 'Serhi',
      lastName: 'Hulko',
      description: '',
      type: 1,
    },
  ],
}

export const reducer = createReducer<IExampleInitialState>('example', initialState, {
  [SUBMIT_EXAMPLE_FORM_SUCCESS]: (state, { payload }: ISubmitExampleFormSuccessPayload) => ({
    users: state.users.concat([payload]),
  }),
  [GET_USERS_ATTEMPT]: () => ({ showLoader: true }),
  [GET_USERS_SUCCESS]: (state, { users }) => ({
    showLoader: false,
    users: state.users.concat(users),
  }),
})
