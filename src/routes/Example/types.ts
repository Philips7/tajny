import { InjectedFormProps } from 'redux-form'
import { Action } from 'redux'

// import { IState } from 'types'

export interface IExampleProps extends InjectedFormProps<any, any> {
  showLoader: boolean,
  isAuthenticated: boolean,
  authAttempt: () => void,
  getNewUsersAttempt: () => void,
  users: IExampleFormValues[],
}

export interface IExampleInitialState {
  showLoader: boolean,
  users: IExampleFormValues[],
}

export interface IExampleForm {
  values: IExampleFormValues,
}

export interface IExampleFormValues {
  firstName: string,
  lastName: string,
  description: string,
  type: number,
}

export interface ISubmitExampleFormSuccessPayload {
  type: string,
  payload: IExampleFormValues,
}

export interface IUsersListProps {
  showLoader: boolean,
  users: IExampleFormValues[],
}

export interface IGetUsers {
  firstName: string,
  lastName: string,
  description: string,
  type: string | number,
}

export interface IGetUsersSuccess {
  (users: IExampleFormValues[]): Action & { users: IExampleFormValues[] },
}
