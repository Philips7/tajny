import Loadable from 'react-loadable'
import * as React from 'react'
import { Loader } from 'components/Loader'

export const Example = Loadable.Map({
  loader: {
    ExampleModule: () => import(/* webpackChunkName: "Route Example" */ './components/Example'),
  },
  loading: Loader,
  render: ({ ExampleModule: { Example } }: any, props) => <Example {...props} />,
})
