import Loadable from 'react-loadable'
import * as React from 'react'
import { Loader } from 'components/Loader'

export const NotFound = Loadable.Map({
  loader: {
    NotFoundModule: () => import(/* webpackChunkName: "Route NotFound" */ './components/NotFound/NotFound'),
  },
  loading: Loader,
  render: ({ NotFoundModule: { NotFound } }: any, props) => (<NotFound {...props} />),
})
