import { applyMiddleware, combineReducers, compose, createStore as reduxCreateStore } from 'redux'
import createHistory from 'history/createBrowserHistory'
import { routerMiddleware, routerReducer as routing } from 'react-router-redux'
import thunk from 'redux-thunk'
import { reducer as form } from 'redux-form'
import { errorHandlerMiddleware } from './errorHandler'
import { createResponsiveStateReducer, responsiveStoreEnhancer } from 'redux-responsive'
import { get, getOnce, patch, post, put } from './api'
import { IStore } from 'types'

let storeIsCreated = false

let store: IStore = {
  makeRootReducer: null,
  asyncReducers: null,
  injectReducer: null,
  replaceReducer: null,

  dispatch: null,
  getState: () => null,
  subscribe: null,
}

export const history = createHistory()

interface ICreateStore {
  (data: { makeRootReducer: any, initialState: any, enhancers: any, middleware: any }): any
}

export const createStore: ICreateStore = ({ makeRootReducer, initialState = {}, enhancers = [], middleware = [] }): IStore => {
  if (process.env.NODE_ENV === 'development') {
    const devToolsExtension = window['devToolsExtension']

    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension())
    }
  }

  const rootReducer = makeRootReducer()
  const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers)
  store = reduxCreateStore(rootReducer, initialState, composedEnhancers)

  store.makeRootReducer = makeRootReducer
  store.asyncReducers = {}
  store.injectReducer = (key: string, reducer: any) => {
    if (!store.asyncReducers[key]) {
      store.asyncReducers[key] = reducer
      store.replaceReducer(store.makeRootReducer(store.asyncReducers))
    } else {
      console.warn(`Reducer ${name} has been injected before`)
    }
  }

  storeIsCreated = true

  return store
}

const makeRootReducer = (asyncReducers: any) => combineReducers({
  ...asyncReducers,
  routing,
  form,
  browser: createResponsiveStateReducer({
    extraSmall: 544,
    small: 767,
    medium: 991,
    large: 1199,
    extraLarge: 1200,
  }),
})

export const getStore = (): any => storeIsCreated
  ? store
  : createStore({
    makeRootReducer,
    initialState: {},
    enhancers: [
      responsiveStoreEnhancer,
    ],
    middleware: [
      thunk.withExtraArgument({ get, getOnce, post, put, patch }),
      routerMiddleware(history),
      errorHandlerMiddleware,
    ],
  })
