import * as qs from 'qs'
import { getCredentials } from 'utils/auth'
import { isEmpty } from 'ramda'

const DEFAULT_BASE_URL = ''
const DEFAULT_CREDENTIALS: RequestCredentials = 'omit'
const DEFAULT_MODE: RequestMode = 'cors'

const GET_ONCE_CACHE = {}

enum ContentTypesEnum {
  PLAIN = 0,
  JSON = 1,
  FORM_DATA = 2,
}

export const contentTypes = {
  PLAIN: 0,
  JSON: 1,
  FORM_DATA: 2,
  // TODO multipart/form-data
}

function prepareUrl (path: string, query: string, baseUrl = DEFAULT_BASE_URL) {
  let url = `${baseUrl}${path}`
  const queryString = qs.stringify(query)
  if (queryString) {
    url = `${url}?${queryString}`
  }
  return url
}

function prepareHeaders (contentType: ContentTypesEnum): HeadersInit {
  const { accessToken, tokenType } = getCredentials()
  const headers = new Headers()

  headers.append('Authorization', `${tokenType} ${accessToken}`)
  headers.append('Accept', 'application/json, application/xml, text/plain, text/html, *.*')

  contentTypes.PLAIN === contentType && headers.append('Content-Type', 'text/plain')
  contentTypes.JSON === contentType && headers.append('Content-Type', 'application/json')
  contentTypes.FORM_DATA === contentType && headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')

  return headers
}

function prepareBody (data: any, contentType: ContentTypesEnum) {
  if (data === '') {
    return ''
  }

  switch (contentType) {
    case contentTypes.JSON:
      return JSON.stringify(data)
    case contentTypes.FORM_DATA:
      // const searchParams = new URLSearchParams()
      // Object.keys(data).forEach((key) => {
      //   searchParams.append(key, data[key])
      // })
      // return searchParams.toString()
      return data // TODO
    // TODO multipart/form-data
    default:
      return data
  }
}

const request = (method: string) => async ({
  path = '',
  query = '',
  data = '',
  contentType = contentTypes.JSON,
  credentials = DEFAULT_CREDENTIALS,
  mode = DEFAULT_MODE,
  baseUrl = '',
  customHeaders = {},
}) => {
  const url = prepareUrl(path, query, baseUrl)
  const headers = isEmpty(customHeaders) ? prepareHeaders(contentType) : customHeaders
  const body = prepareBody(data, contentType)

  // TODO catch 401 -> stop requests -> try refresh token -> resend requests
  const response = await fetch(url, {
    method,
    headers,
    body: body || undefined,
    mode,
    credentials,
  })

  const responseData = await response.json()

  if (!response.ok) {
    return Promise.reject({ status: response.status, data: responseData })
  }

  return responseData
}

export const get = request('GET')
export const post = request('POST')
export const put = request('PUT')
export const patch = request('PATCH')
export const getOnce = ({ path = '', query = '' }) => {
  const url = prepareUrl(path, query)

  if (!GET_ONCE_CACHE[url]) {
    GET_ONCE_CACHE[url] = get({ path, query })
  }

  return GET_ONCE_CACHE[url]
}
