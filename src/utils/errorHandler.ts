import { Middleware } from 'redux'

export const errorHandlerMiddleware = (): Middleware => (next: any) => (action: any) => {
  try {
    return next(action)
  } catch (e) {
    logError(e)
  }
}

export const logError = (e: Error): void => {
  console.log(':(', e)
}
