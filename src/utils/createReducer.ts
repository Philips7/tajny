import { getStore } from './store'
import { Reducer } from 'redux'

interface IHandlers<State> {
  [key: string]: (state: State, action: any) => any
}

interface ICreateReducer {
  <T>(name: string, defaultState: T, handlers: IHandlers<T>): Reducer<T>,
}

export const createReducer: ICreateReducer = (name, defaultState, handlers) => {
  const reducer = (state = defaultState, action = { type: '' }) => {
    const handler = handlers[action.type]
    if (handler) {
      const result = handler(state, action)
      return Object.assign({}, state, result) // https://github.com/Microsoft/TypeScript/pull/13288
    }
    return state
  }

  getStore().injectReducer(name, reducer)

  return reducer
}
