import { Auth0DecodedHash } from 'auth0-js'

export interface ICredentials {
  accessToken: Auth0DecodedHash['accessToken'],
  expiresIn: Auth0DecodedHash['expiresIn'],
  tokenType: Auth0DecodedHash['tokenType'],
  idToken: Auth0DecodedHash['idToken'],
}

export const setCredentials = (data: ICredentials): void => {
  localStorage.setItem('accessToken', data.accessToken || '')
  localStorage.setItem('expiresIn', `${data.expiresIn || 7200}`)
  localStorage.setItem('tokenType', `${data.tokenType || ''}`)
  localStorage.setItem('idToken', `${data.idToken || ''}`)
}

export const getCredentials = (): ICredentials => ({
  accessToken: localStorage.getItem('accessToken') || '',
  expiresIn: parseInt(localStorage.getItem('expiresIn') || '7200', 10),
  tokenType: localStorage.getItem('tokenType') || '',
  idToken: localStorage.getItem('idToken') || '',
})

export const removeCredentials = (): void => {
  localStorage.removeItem('accessToken')
  localStorage.removeItem('expiresIn')
  localStorage.removeItem('tokenType')
  localStorage.removeItem('idToken')
}
