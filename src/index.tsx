import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { register } from 'utils/registerServiceWorker'
import { App } from 'components/App'
import { Provider } from 'react-redux'
import { getStore } from 'utils/store'
// es5 target doesn't support eg find
import 'core-js/es6/array'

const store = getStore()

ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>,
  document.getElementById('root') as HTMLElement,
)

console.log(`%c 🚧 Cebula ${process.env.RELEASE} 🚧 %c\nConfig: ${process.env.CONFIG_ENV} \nEnv: ${process.env.NODE_ENV}`, 'color: hotpink; font-family: "Comic Sans MS", "Comic Sans", cursive; font-size: 40px', '')

register()
