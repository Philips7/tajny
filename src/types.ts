import { IExampleForm, IExampleInitialState } from 'routes/Example/types'
import { INotificationsInitialState } from 'components/Notifications/types'
import { IUserInitialState } from 'components/App/types'
import { IHomeInitialState } from './routes/Home/types'

export interface IState {
  home?: IHomeInitialState,
  example?: IExampleInitialState,
  notifications?: INotificationsInitialState,
  user?: IUserInitialState,
  routing: {
    location: null | {
      pathname: string,
    }
  },
  form: {
    example?: IExampleForm,
  }
}

export interface IStore {
  makeRootReducer?: any,
  asyncReducers?: any,
  injectReducer?: any,
  replaceReducer?: any,

  dispatch: any,
  getState: any,
  subscribe: any,
}
